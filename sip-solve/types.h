// Author(s): D. Simmons-Duffin, April-October 2013

#ifndef SIP_SOLVE_TYPES_H_
#define SIP_SOLVE_TYPES_H_

#include <mblas_mpfr.h>
#include <mlapack_mpfr.h>

typedef mpackint Integer;
typedef mpreal Real;

#define FLOOR(x) (mpfr_get_si(x, MPFR_RNDN))

#endif  // SIP_SOLVE_TYPES_H_
