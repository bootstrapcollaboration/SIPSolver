// Author(s): D. Simmons-Duffin, April-October 2013

#ifndef SIP_SOLVE_SIPSOLVER_H_
#define SIP_SOLVE_SIPSOLVER_H_

#include <utility>
#include <vector>
#include "boost/function.hpp"
#include "types.h"
#include "util.h"
#include "BasisElt.h"
#include "BasisMatrix.h"
#include "SemiInfiniteProgram.h"
#include "DampedPartialFraction.h"
#include "ParabolaSearch.h"

using std::pair;
using std::vector;

// Maximium number of iterations to run the simplex algorithm before
// throwing an error
const int MAX_ITERATIONS = 500000;

// Number of iterations after which to print info about the progress
// of the simplex algorithm
const int PRINT_INFO_ITERATIONS = 50;

// Number of iterations after which to save a checkpoint to a file.
// When the program is restarted, it will resume from the last saved
// checkpoint
const int SAVE_CHECKPOINT_ITERATIONS = 500;

// Terminate the simplex method when the minimum reduced cost is
// greater than this value
const Real MIN_REDUCED_COST_EPSILON = -1e-60;

class SIPSolver {
 public:
  int dim;
  SemiInfiniteProgram sip;
  vector<BasisElt> basis;
  vector< vector<Real> > basisVectors;
  vector<Real> costVec;

  SIPSolver(const SemiInfiniteProgram &s,
            const vector<BasisElt> &initialBasis);
  static SIPSolver feasibility(const SemiInfiniteProgram &s,
                               const vector<BasisElt>& workingBasis = vector<BasisElt>());

  Real objective();
  vector<Real> coefficients();

  typedef boost::function<void(const vector<BasisElt>&)> SIPBasisSaver;

  Real run(const int maxIterations,
           const SIPBasisSaver &saveCheckpoint = unaryNoop<vector<BasisElt> >);

 private:
  BasisMatrix basisMatrix;
  ParabolaSearchParallel<DampedPartialFraction> parabolaSearch;
  void computeCostVec();
  pair<BasisElt, Real> minimizePoints(BasisElt curMinLabel, Real curMin);
  pair<BasisElt, Real> minimizeCurves(BasisElt curMinLabel, Real curMin);
  pair<BasisElt, Real> minimizeReducedCost();
  int leavingBasisElt(const vector<Real> &enteringBasisVector);
  void printInfo(int iteration, Real minReducedCost);
};

#endif  // SIP_SOLVE_SIPSOLVER_H_
