// Author(s): D. Simmons-Duffin, April-October 2013

#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>
#include "boost/format.hpp"
#include "BasisElt.h"

using std::ostream;
using std::ostringstream;
using std::sort;

bool operator<(const BasisElt &a, const BasisElt &b) {
  if (a.ty == SIP_CURVE && b.ty == SIP_CURVE) {
    return (a.n < b.n) || (a.n == b.n && a.x < b.x);
  } else if (a.ty == SIP_POINT && b.ty == SIP_POINT) {
    return a.n < b.n;
  } else {
    return a.ty == SIP_CURVE;
  }
}

// Print basis elements (either individual points or points on curves)
ostream& operator<<(ostream& os, const BasisElt& b) {
  ostringstream oss;
  oss.precision(16);
  if (b.ty == SIP_POINT) {
    oss << "p" << b.n;
  } else {
    oss << "c" << b.n << "(" << b.x << ")";
  }
  os << oss.str();
  return os;
}

ostream& printBasis(ostream& os, const vector<BasisElt> &unsortedBasis) {
  vector<BasisElt> basis = unsortedBasis;
  sort(basis.begin(), basis.end());
  for (unsigned int i = 0; i < (basis.size() + 3)/4; i++) {
    os << boost::format("%-28s") % basis[4*i];
    if (4*i + 1 < basis.size())
      os << boost::format("%-28s") % basis[4*i + 1];
    if (4*i + 2 < basis.size())
      os << boost::format("%-28s") % basis[4*i + 2];
    if (4*i + 3 < basis.size())
      os << boost::format("%-28s") % basis[4*i + 3];
    os << "\n";
  }
  os.flush();
  return os;
}
