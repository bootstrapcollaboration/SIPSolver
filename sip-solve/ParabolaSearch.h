// Author(s): D. Simmons-Duffin, April-October 2013

#ifndef SIP_SOLVE_PARABOLASEARCH_H_
#define SIP_SOLVE_PARABOLASEARCH_H_

#include <algorithm>
#include <queue>
#include <iostream>
#include <utility>
#include <vector>
#include "boost/tuple/tuple.hpp"
#include "types.h"

using std::cout;
using std::queue;
using std::vector;

// In a parabola search, we search smaller and smaller intervals until
// a parabolic approximation of the first derivative at the center of
// an interval agrees with the first derivative on the endpoints of
// the interval to within this accuracy.
const Real FIRST_DERIVATIVE_ESTIMATE_ACCURACY = 0.1;

template <class F>
class ParabolaSearch {
 public:
  // Search accuracy in x
  const Real searchThresh;

  Real xGlobalMin;
  Real fGlobalMin;

  int numIntervalTests;
  int numNewtonSearches;

  explicit ParabolaSearch(const Real &searchThresh):
    searchThresh(searchThresh),
    numIntervalTests(0),
    numNewtonSearches(0) {}

  inline void maybeUpdateGlobalMin(const Real &x,
                                   const Real &f,
                                   bool force = false) {
    if (f < fGlobalMin || force) {
      xGlobalMin = x;
      fGlobalMin = f;
    }
  }

  // Use a hybrid of newton's method and bisection to find a local
  // minimum of f within the bracket [xNeg, xPos], to within
  // searchThresh.  xTest is an initial test point that must lie
  // within [xNeg, xPos].  The derivative of f must change sign from
  // negative to positive between xNeg and xPos.
  const Real findLocalMinimum(const F &f,
                              const Real &xNeg,
                              const Real &xPos,
                              const Real &xTest) const {
    Real neg  = xNeg;
    Real pos  = xPos;
    Real test = xTest;
    while (true) {
      const Real fFirstDerivTest  = f.derivative1(test);
      const Real fSecondDerivTest = f.derivative2(test);

      if (fFirstDerivTest > 0)
        pos = test;
      else
        neg = test;

      Real testNew = test - fFirstDerivTest / fSecondDerivTest;

      if (testNew > max(pos, neg) || testNew < min(pos, neg))
        testNew = (pos + neg)/2;

      if (abs(testNew - test) < searchThresh)
        return testNew;

      test = testNew;
    }
  }

  void recurse(const F &f,
               const Real &xLeft,
               const Real &bLeft,
               const Real &xRight,
               const Real &bRight) {
    numIntervalTests++;
    const Real xMid = (xLeft + xRight)/2;
    Real bMid, cMid, dMid;

    // Compute the first, second, and third derivatives of f and store
    // them in bMid, cMid, and dMid, respectively.  Equivalent to (but
    // faster than):
    //
    // bMid = f.derivative1(xMid);
    // cMid = f.derivative2(xMid);
    // dMid = f.derivative3(xMid);
    f.multiDerivatives(xMid, bMid, cMid, dMid);

    const Real halfWidth = (xMid - xLeft);
    const Real evenPiece = bMid + dMid * halfWidth * halfWidth / 2;
    const Real oddPiece  = cMid * halfWidth;
    const Real bLeftEstimate  = evenPiece - oddPiece;
    const Real bRightEstimate = evenPiece + oddPiece;

    if (abs(bLeftEstimate  - bLeft) /(abs(bLeft)  + abs(bLeftEstimate))  <
        FIRST_DERIVATIVE_ESTIMATE_ACCURACY/2
        &&
        abs(bRightEstimate - bRight)/(abs(bRight) + abs(bRightEstimate)) <
        FIRST_DERIVATIVE_ESTIMATE_ACCURACY/2) {
      // Interval is "good"
      if (bLeft < 0 && bRight > 0) {
        numNewtonSearches++;
        const Real xLocalMin = findLocalMinimum(f, xLeft, xRight, xMid);
        maybeUpdateGlobalMin(xLocalMin, f(xLocalMin));
      }
    } else {
      recurse(f, xLeft, bLeft, xMid,   bMid);
      recurse(f, xMid,  bMid,  xRight, bRight);
    }
  }

  pair<Real, Real> minimize(const F &f) {
    // iGlobalMin < 0 indicates that the minima are un-initialized, in
    // which case we should force-update the global minimum
    maybeUpdateGlobalMin(f.xLeft,  f(f.xLeft), true);
    maybeUpdateGlobalMin(f.xRight, f(f.xRight));

    const Real bLeft  = f.derivative1(f.xLeft);
    const Real bRight = f.derivative1(f.xRight);

    recurse(f, f.xLeft, bLeft, f.xRight, bRight);
    return pair<Real, Real>(xGlobalMin, fGlobalMin);
  }

  boost::tuple<int, Real, Real> minimizeFunctions(vector<F> fs,
                                                  Real fInitialMin) {
    Real fMin = fInitialMin;
    Real xMin = 0;
    int  iMin = -1;

    for (unsigned int i = 0; i < fs.size(); i++) {
      pair<Real, Real> p = minimize(fs[i]);
      if (p.second < fMin) {
        fMin = p.second;
        xMin = p.first;
        iMin = i;
      }
    }

    return boost::tuple<int, Real, Real>(iMin, xMin, fMin);
  }
};

template <class F>
class ParabolaSearchParallel {
 public:
  Real searchThresh;
  int numIntervalTests;
  int numNewtonSearches;
  vector<ParabolaSearch<F> > parabolaSearches;

  ParabolaSearchParallel(Real searchThresh, int numFunctions):
    searchThresh(searchThresh),
    numIntervalTests(0),
    numNewtonSearches(0),
    parabolaSearches(numFunctions, ParabolaSearch<F>(searchThresh)) {}

  boost::tuple<int, Real, Real> minimizeFunctions(vector<F> fs,
                                                  Real fInitialMin) {
    vector<pair<Real, Real> > mins(fs.size());

    // Two threads seems sufficient to get most of the possible
    // performance gains out of this loop.  It may be that more
    // fine-grained parallelism is in order
    #pragma omp parallel for schedule(dynamic)
    for (unsigned int i = 0; i < fs.size(); i++)
      mins[i] = parabolaSearches[i].minimize(fs[i]);

    Real fMin = fInitialMin;
    Real xMin = 0;
    int iMin = -1;
    numIntervalTests = 0;
    numNewtonSearches = 0;

    for (unsigned int i = 0; i < fs.size(); i++) {
      numIntervalTests += parabolaSearches[i].numIntervalTests;
      numNewtonSearches += parabolaSearches[i].numNewtonSearches;
      if (mins[i].second < fMin) {
        fMin = mins[i].second;
        xMin = mins[i].first;
        iMin = i;
      }
    }

    return boost::tuple<int, Real, Real>(iMin, xMin, fMin);
  }
};

#endif  // SIP_SOLVE_PARABOLASEARCH_H_
