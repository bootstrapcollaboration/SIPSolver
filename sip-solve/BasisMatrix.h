// Author(s): D. Simmons-Duffin, April-October 2013

#ifndef SIP_SOLVE_BASISMATRIX_H_
#define SIP_SOLVE_BASISMATRIX_H_

#include <vector>
#include "types.h"

using std::vector;

// A BasisMatrix is an invertible matrix build out of basic vectors.
// Only an LU decomposition is actually stored.
class BasisMatrix {
  int dim;

  // An LU Decomposition of the basis in column-major order (for
  // consistency with lapack conventions)
  Real* LUDecomposition;

  // Needed as input to the lapack LU decomposition function
  Integer *ipiv;
  // Nedded as input to the lapack LU decomposition function
  Integer info;

 public:
  explicit BasisMatrix(int n);
  ~BasisMatrix();

  // Load the columns of our matrix with the vectors vs, and perform
  // an LU decomposition.
  void loadVectors(const vector< vector<Real> > &vs);

  // Compute the inverse-transpose of the basis matrix acting on v.
  // Changes v in-place.
  void inverseTransposeInplace(vector<Real> &);

  // Compute the inverse of the basis matrix acting on v.  Changes v
  // in-place.
  void inverseInplace(vector<Real> &);

  // Returns the inverse-transpose of the basis matrix acting on v
  vector<Real> inverseTranspose(const vector<Real> &);

  // Returns the inverse of the basis matrix acting on v
  vector<Real> inverse(const vector<Real> &);
};

#endif  // SIP_SOLVE_BASISMATRIX_H_
