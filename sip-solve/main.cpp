// Author(s): D. Simmons-Duffin, April-October 2013

#include <iostream>
#include <string>
#include <vector>
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/variant.hpp"
#include "parse.h"
#include "SIPSolver.h"
#include "tests.h"
#include "util.h"
#include "maximizeOPECoeff.h"
#include "BoundarySearch.h"

using std::string;
using std::cout;
using boost::filesystem::path;

void runSIPFile(path sipPath, path outPath) {
  SemiInfiniteProgram sip = readSemiInfiniteProgram(sipPath.c_str());
  SIPSolver solver = SIPSolver::feasibility(sip);
  Real obj = solver.run(200000);
  if (obj > 0) {
    cout << "Infeasible; ruled out by functional: \n  ";
    printVector(solver.costVec);
    cout << "\n";
  } else {
    printBasisJson(cout, zipVectors(solver.basis, solver.coefficients()));
  }
}

// Usage:
//
// ./sip-solve <sipFile> search <searchIndex> <searchMin> <searchMax> <searchThresh>
// ./sip-solve <sipFile> maximize <curveIndex> <gap>
// ./sip-solve <sipFile> bound
//
int main(int argc, char **argv) {
  int default_prec = 128;
  mpfr_set_default_prec(default_prec);
  cout.precision(70);
  const char* sipFile = argv[1];

  path sipPath(sipFile), outPath(sipFile), checkpointPath(sipFile);

  const string action = string(argv[2]);
  if (action == "search") {
    outPath.replace_extension("out");
    checkpointPath.replace_extension("checkpoints");
    runSIPFileBoundarySearch(sipPath,
                             outPath,
                             checkpointPath,
                             atoi(argv[3]),
                             Real(argv[4]),
                             Real(argv[5]),
                             Real(argv[6]));
  } else if (action == "maximize") {
    outPath.replace_extension("maximization_out");
    checkpointPath.replace_extension("maximization_checkpoints");
    maximizeOPECoeff(sipPath,
                     outPath,
                     checkpointPath,
                     atoi(argv[3]),
                     Real(argv[4]));
  } else if (action == "bound") {
    runSIPFile(sipPath, outPath);
  }

  // serializeTest();
  // parsingAndMathTest(argv[1]);
  // //bbTest();
  // psTest();
  // //dpTest();
  // polTest();
  // dprTest();
  return 0;
}
