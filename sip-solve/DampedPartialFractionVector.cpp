// Author(s): D. Simmons-Duffin, April-October 2013

#include <vector>
#include "DampedPartialFractionVector.h"

// Evaluate at a real number to get a vector
vector<Real> DampedPartialFractionVector::operator()(const Real &x) const {
  int dim = components.size();
  vector<Real> v(dim);
  for (int i = 0; i < dim; i++) {
    v[i] = components[i](x);
  }
  return v;
}

// The maximum degree of all components
int DampedPartialFractionVector::maxDegree() const {
  int maxDeg = 0;
  for (unsigned int i = 0; i < components.size(); i++) {
    int d = components[i].degree();
    maxDeg = (d > maxDeg) ? d : maxDeg;
  }
  return maxDeg;
}

// Dot product with a vector of real numbers
DampedPartialFraction
DampedPartialFractionVector::dot(const vector<Real> &v) const {
  Real rho              = components[0].rho;
  vector<Real> poles    = components[0].poles;

  Polynomial p;
  p.coeffs.resize(maxDegree() + 1, 0);

  vector<Real> residues;
  residues.resize(poles.size());

  for (unsigned int j = 0; j < components.size(); j++) {
    const vector<Real> coeffs_j   = components[j].p0.coeffs;
    const vector<Real> residues_j = components[j].residues;
    const Real v_j = v[j];

    for (unsigned int i = 0; i < components[j].p0.coeffs.size(); i++)
      p.coeffs[i] += v_j * coeffs_j[i];

    for (unsigned int i = 0; i < poles.size(); i++)
      residues[i] += v_j * residues_j[i];
  }
  DampedPartialFraction f(rho, p, poles, residues, xLeft, xRight);

  return f;
}
