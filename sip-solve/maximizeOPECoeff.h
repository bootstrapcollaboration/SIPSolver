// Author(s): D. Simmons-Duffin, April-October 2013

#ifndef SIP_SOLVE_MAXIMIZEOPECOEFF_H_
#define SIP_SOLVE_MAXIMIZEOPECOEFF_H_

#include <utility>
#include <vector>
#include "boost/filesystem.hpp"
#include "serialize.h"
#include "SIPSolver.h"

using boost::filesystem::path;

enum OPEMaximizationStage { FINDING_FEASIBLE, MAXIMIZING_COEFFICIENT };

typedef pair<OPEMaximizationStage, vector<BasisElt> > OPEMaximizationCheckpoint;

BOOST_CLASS_TRACKING(OPEMaximizationCheckpoint, boost::serialization::track_never)

void maximizeOPECoeff(path sipPath, path outPath, path checkpointPath, int curveIndex, Real gap);

#endif  // SIP_SOLVE_MAXIMIZEOPECOEFF_H_
