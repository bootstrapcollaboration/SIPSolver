// Author(s): D. Simmons-Duffin, April-October 2013

#include <numeric>
#include <vector>
#include <utility>
#include "boost/algorithm/cxx11/all_of.hpp"
#include "boost/tuple/tuple.hpp"
#include "SIPSolver.h"

using std::cout;
using std::endl;
using std::inner_product;
using boost::tuple;
using boost::algorithm::all_of_equal;

SIPSolver::SIPSolver(const SemiInfiniteProgram &sip,
                     const vector<BasisElt> &initialBasis):
  dim(sip.dim()),
  sip(sip),
  basis(initialBasis),
  basisVectors(dim),
  costVec(dim),
  basisMatrix(dim),
  parabolaSearch(1e-60, sip.curves.size()) {
  // Load initial basis into basisVectors
  for (int i = 0; i < dim; i++)
    basisVectors[i] = sip.vec(basis[i]);
}


inline Real dot(const vector<Real> &u, const vector<Real> &v) {
  return inner_product(u.begin(), u.end(), v.begin(), Real(0));
}

Real SIPSolver::objective() {
  return -dot(costVec, sip.norm);
}

vector<Real> SIPSolver::coefficients() {
  return basisMatrix.inverse(sip.norm);
}

void SIPSolver::computeCostVec() {
  for (int i = 0; i < dim; i++)
    costVec[i] = -sip.obj(basis[i]);
  basisMatrix.inverseTransposeInplace(costVec);
}

pair<BasisElt, Real> SIPSolver::minimizePoints(BasisElt curMinElt, Real curMin) {
  for (unsigned int i = 0; i < sip.points.size(); i++) {
    Real reducedCost = sip.pointObj[i] + dot(costVec, sip.points[i]);
    if (reducedCost < curMin) {
      curMin    = reducedCost;
      curMinElt = BasisElt::point(i);
    }
  }
  return pair<BasisElt, Real>(curMinElt, curMin);
}

pair<BasisElt, Real> SIPSolver::minimizeCurves(BasisElt curMinElt, Real curMin) {
  if (all_of_equal(costVec.begin(), costVec.end(), Real(0))) {
    // If costVec vanishes identically, then don't bother doing the
    // minimization via a parabola search
    if (0 < curMin) {
      curMin = 0;
      curMinElt = BasisElt::curve(0, sip.curves[0].xLeft);
    }
  } else {
    // Otherwise, we dot each curve with costVec and minmize the
    // resulting functions with a parabola search
    vector<DampedPartialFraction> fs;
    for (unsigned int i = 0; i < sip.curves.size(); i++)
      fs.push_back(sip.curves[i].dot(costVec));

    tuple<int, Real, Real> t = parabolaSearch.minimizeFunctions(fs, curMin);
    int iMin  = t.get<0>();
    Real xMin = t.get<1>();
    Real fMin = t.get<2>();

    if (fMin < curMin) {
      curMin    = fMin;
      curMinElt = BasisElt::curve(iMin, xMin);
    }
  }

  return pair<BasisElt, Real>(curMinElt, curMin);
}

pair<BasisElt, Real> SIPSolver::minimizeReducedCost() {
  pair<BasisElt, Real> p = pair<BasisElt, Real>(BasisElt::point(-1), 0);
  p = minimizePoints(p.first, p.second);
  p = minimizeCurves(p.first, p.second);
  return p;
}

int SIPSolver::leavingBasisElt(const vector<Real> &enteringBasisVector) {
  vector<Real> curCoords = basisMatrix.inverse(sip.norm);
  vector<Real> newCoords = basisMatrix.inverse(enteringBasisVector);

  Real minCoordShift;
  int minCoordShiftIndex = -1;

  for (int i = 0; i < dim; i++) {
    if (newCoords[i] > 0) {
      Real coordShift = curCoords[i]/newCoords[i];
      if (minCoordShiftIndex == -1 || coordShift < minCoordShift) {
        minCoordShift = coordShift;
        minCoordShiftIndex = i;
      }
    }
  }
  return minCoordShiftIndex;
}

Real SIPSolver::run(const int maxIterations,
                    const boost::function<void(const vector<BasisElt>&)> &saveCheckpoint) {
  for (int iteration = 1; iteration <= maxIterations; iteration++) {
    if (iteration % SAVE_CHECKPOINT_ITERATIONS == 0)
      saveCheckpoint(basis);

    basisMatrix.loadVectors(basisVectors);
    computeCostVec();
    pair<BasisElt, Real> p = minimizeReducedCost();
    BasisElt enteringBasisElt = p.first;
    Real minReducedCost       = p.second;

    if (iteration % PRINT_INFO_ITERATIONS == 0)
      printInfo(iteration, minReducedCost);

    if (minReducedCost >= MIN_REDUCED_COST_EPSILON) {
      cout << "SIP solver finished in " << iteration << " iterations." << endl;
      return objective();
    } else {
      vector<Real> enteringBasisVector = sip.vec(enteringBasisElt);
      int n = leavingBasisElt(enteringBasisVector);

      if (n < 0) {
        throw("Unbounded semi-infinite program");
      } else {
        basis[n]        = enteringBasisElt;
        basisVectors[n] = enteringBasisVector;
      }
    }
  }
  throw("Max iterations exceeded");
}

SIPSolver SIPSolver::feasibility(const SemiInfiniteProgram &s,
                                 const vector<BasisElt>& workingBasis) {
  SemiInfiniteProgram f = s;
  vector<BasisElt> initialBasis;
  for (int i = 0; i < f.dim(); i++) {
    vector<Real> slackVec(s.dim());
    slackVec[i] = 1;
    f.points.push_back(slackVec);
    f.pointObj.push_back(f.norm[i] != 0 ? sgn(f.norm[i]) : 1);
    initialBasis.push_back(BasisElt::point(f.points.size() - 1));
  }
  if (workingBasis.size() > 0)
    return SIPSolver(f, workingBasis);
  else
    return SIPSolver(f, initialBasis);
}

void SIPSolver::printInfo(int iteration, Real minReducedCost) {
  cout << "-------------------------------------------------------" << endl;
  cout << "iteration      : " << iteration << endl;
  cout << "objective      : " << objective() << endl;
  cout << "minReducedCost : " << minReducedCost << endl;
  cout << "intervalTests  : " <<
    static_cast<float>(parabolaSearch.numIntervalTests)/iteration << "/iteration" << endl;
  cout << "newtonSearches : " <<
    static_cast<float>(parabolaSearch.numNewtonSearches)/iteration << "/iteration" << endl;
  cout << "basis          :" << endl;
  printBasis(cout, basis);
}
