// Author(s): D. Simmons-Duffin, April-October 2013

#ifndef SIP_SOLVE_BOUNDARYSEARCH_H_
#define SIP_SOLVE_BOUNDARYSEARCH_H_

#include <utility>
#include <vector>
#include "boost/filesystem.hpp"
#include "boost/function.hpp"
#include "boost/phoenix.hpp"
#include "boost/phoenix/core.hpp"
#include "boost/phoenix/function.hpp"
#include "boost/phoenix/bind/bind_member_function.hpp"
#include "SIPSolver.h"

using boost::filesystem::path;
using boost::phoenix::bind;
using boost::phoenix::arg_names::arg1;

const int INFEASIBLE_TO_FEASIBLE_TIME_RATIO = 200;

class BoundarySearch {
 public:
  Real xMin;
  Real xMax;
  vector<BasisElt> feasibleBasis;
  vector<Real>     feasibleCoords;
  vector<BasisElt> workingBasis;

  // Default constructor so that we can serialize BoundarySearch's to
  // and from an archive
  BoundarySearch() {}

  // A search with an uninitialized feasibleBasis.  This will always
  // be the case at the beginning of a search, before we find a
  // feasible basis by solving an auxiliary problem with slack
  // variables (see 'findFeasibleBasis').  To test whether
  // 'feasibleBasis' is initialized, we'll check it's vector size() --
  // a zero-length vector means we should run 'findFeasibleBasis'.
  BoundarySearch(Real xMin,
                 Real xMax):
    xMin(xMin),
    xMax(xMax) {}

  // A search with a feasibleBasis found that is ready to continue
  // with a smaller search window
  BoundarySearch(Real xMin,
                 Real xMax,
                 vector<BasisElt> feasibleBasis,
                 vector<Real> feasibleCoords):
    xMin(xMin),
    xMax(xMax),
    feasibleBasis(feasibleBasis),
    feasibleCoords(feasibleCoords) {}

  typedef boost::function<void(const BoundarySearch&)> BoundarySearchSaver;
  typedef boost::function<void(const vector<BasisElt> &)> SIPBasisSaver;

  void saveSIPBasis(const BoundarySearchSaver &saveBoundarySearch,
                    const vector<BasisElt> &basis) {
    workingBasis = basis;
    saveBoundarySearch(*this);
  }

  Real runCheckpointedSolver(SIPSolver *solver,
                             int maxIterations,
                             const BoundarySearchSaver &saveBoundarySearch) {
    const SIPBasisSaver saveBasis =
      bind(&BoundarySearch::saveSIPBasis, this, saveBoundarySearch, arg1);
    return solver->run(MAX_ITERATIONS, saveBasis);
  }

  SIPSolver minimizeOPECoeffsBelow(const SemiInfiniteProgram &oldSIP,
                                   const Real &xMinNew);

  void findFeasibleBasis(const SemiInfiniteProgram &sip,
                         const BoundarySearchSaver &saveBoundarySearch);

  pair<Real, vector< pair<BasisElt, Real> > >
  run(SemiInfiniteProgram sip,
      Real thresh,
      const BoundarySearchSaver &saveCheckpoint);
};

void runSIPFileBoundarySearch(path sipPath,
                              path outPath,
                              path checkpointPath,
                              int searchIndex,
                              Real xMin,
                              Real xMax,
                              Real thresh);

#endif  // SIP_SOLVE_BOUNDARYSEARCH_H_
