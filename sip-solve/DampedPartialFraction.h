// Author(s): D. Simmons-Duffin, April-October 2013

#ifndef SIP_SOLVE_DAMPEDPARTIALFRACTION_H_
#define SIP_SOLVE_DAMPEDPARTIALFRACTION_H_

#include <vector>
#include "types.h"
#include "Polynomial.h"

// A function of the form
//
// rho^x (P(x) + c_1/(x-x_1) + x_2/(x-x_2) + ...)
//
// where P(x) is a polynomial and rho < 1 is a number.
class DampedPartialFraction {
 public:
  Real rho;
  Real logRho;
  Real xLeft;
  Real xRight;
  Polynomial p0;
  Polynomial p1;
  Polynomial p2;
  Polynomial p3;
  vector<Real> poles;
  vector<Real> residues;
  DampedPartialFraction();
  DampedPartialFraction(Real rho,
                        Polynomial p,
                        vector<Real> poles,
                        vector<Real> residues,
                        Real xLeft = 0,
                        Real xRight = 40);
  int degree() const { return p0.degree(); }
  Real           operator()(const Real &)           const;
  interval<Real> operator()(const interval<Real> &) const;
  Real derivative1(const Real &) const;
  Real derivative2(const Real &) const;
  Real derivative3(const Real &) const;
  void multiDerivatives(const Real &x, Real &, Real &, Real &) const;
  void precomputeDerivatives();

 private:
  Real singlePoleDerivative1(const Real &r, const Real &a, const Real &x) const;
  Real singlePoleDerivative2(const Real &r, const Real &a, const Real &x) const;
  Real singlePoleDerivative3(const Real &r, const Real &a, const Real &x) const;
  Polynomial dampedPolynomialDerivative(const Polynomial &q) const;
};

#endif  // SIP_SOLVE_DAMPEDPARTIALFRACTION_H_
