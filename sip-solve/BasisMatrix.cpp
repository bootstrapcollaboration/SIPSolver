// Author(s): D. Simmons-Duffin, April-October 2013

#include <vector>
#include "types.h"
#include "BasisMatrix.h"

using std::vector;

BasisMatrix::BasisMatrix(int d) {
  dim = d;
  LUDecomposition = new Real[dim*dim];
  ipiv            = new Integer[dim];
}

BasisMatrix::~BasisMatrix() {
  delete[] LUDecomposition;
  delete[] ipiv;
}

void BasisMatrix::loadVectors(const vector< vector<Real> > &vs) {
  // Because lapack uses Fortran conventions, our matrix is in
  // column-major order.
  for (int i = 0; i < dim; i++) {
    const vector<Real> v = vs[i];
    for (int j = 0; j < dim; j++) {
      LUDecomposition[dim*i + j] = v[j];
    }
  }
  // Perform LU decomposition
  Rgetrf(dim, dim, LUDecomposition, dim, ipiv, &info);
}

void BasisMatrix::inverseTransposeInplace(vector<Real> &v) {
  Rgetrs("T", dim, 1, LUDecomposition, dim, ipiv, &v[0], dim, &info);
}

void BasisMatrix::inverseInplace(vector<Real> &v) {
  Rgetrs("N", dim, 1, LUDecomposition, dim, ipiv, &v[0], dim, &info);
}

vector<Real> BasisMatrix::inverseTranspose(const vector<Real> &v) {
  vector<Real> result = v;
  inverseTransposeInplace(result);
  return result;
}

vector<Real> BasisMatrix::inverse(const vector<Real> &v) {
  vector<Real> result = v;
  inverseInplace(result);
  return result;
}
