// Author(s): D. Simmons-Duffin, April-October 2013

#ifndef SIP_SOLVE_UTIL_H_
#define SIP_SOLVE_UTIL_H_

#include <iostream>
#include <vector>
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include "BasisElt.h"

using std::pair;
using boost::filesystem::path;

void printBasisJson(ostream &out, const vector< pair<BasisElt, Real> >& basis);

void printBoundaryJson(ostream &out, const pair<Real, vector< pair<BasisElt, Real> > >& bdry);

path backupFile(path p);

template <class A, class B>
vector<pair<A, B> > zipVectors(const vector<A> &as, const vector<B> &bs) {
  vector<pair<A, B> > pairs(0);
  for (unsigned int i = 0; i < as.size(); i++)
    pairs.push_back(pair<A, B>(as[i], bs[i]));
  return pairs;
}

template <class A>
void unaryNoop(A a) {
  return;
}

#endif  // SIP_SOLVE_UTIL_H_
