// Author(s): D. Simmons-Duffin, April-October 2013

#ifndef SIP_SOLVE_BB_H_
#define SIP_SOLVE_BB_H_

#include <algorithm>
#include <iostream>
#include <queue>
#include <vector>
#include "boost/numeric/interval.hpp"
#include "boost/tuple/tuple.hpp"
#include "types.h"

using std::cout;
using std::queue;
using std::vector;
using boost::numeric::interval;

template <class F>
class BranchAndBound {
 public:
  // Determine x value of the minimum within searchThresh
  const Real searchThresh;
  // Switch from branch and bound to newton's method when intervals
  // get smaller than localSearchThresh
  const Real localSearchThresh;

  int numIntervalTests;
  int numNewtonSearches;

  // Index of the function that achieves the current minimum
  int  iGlobalMin;
  // Argument of the function that achieves the current minimum
  Real xGlobalMin;
  // Value of the current minimum
  Real fGlobalMin;

  inline void maybeUpdateGlobalMin(const int &i,
                                   const Real &x,
                                   const Real &f,
                                   bool force = false) {
    if (f < fGlobalMin || force) {
      iGlobalMin = i;
      xGlobalMin = x;
      fGlobalMin = f;
    }
  }

  class MinSearch {
   public:
    const Real xLeft;
    const Real fLeft;
    const Real xRight;
    const Real fRight;
    const F &f;
    const int fIndex;
    MinSearch(const Real &xLeft,
              const Real &fLeft,
              const Real &xRight,
              const Real &fRight,
              const F &f,
              const int &fIndex):
      xLeft(xLeft),
      fLeft(fLeft),
      xRight(xRight),
      fRight(fRight),
      f(f),
      fIndex(fIndex) {}

    // Use a hybrid of newton's method and bisection to find a local
    // minimum of f within the bracket [xNeg, xPos], to within
    // searchThresh.  xTest is an initial test point that must lie
    // within [xNeg, xPos].  The derivative of f must change sign from
    // negative to positive between xNeg and xPos.
    Real findLocalMinimum(const F &f,
                          const Real &xNeg,
                          const Real &xPos,
                          const Real &xTest,
                          const Real &searchThresh) const {
      const Real fFirstDerivTest  = f.derivative1(xTest);
      const Real fSecondDerivTest = f.derivative2(xTest);
      Real xNegNew = xNeg;
      Real xPosNew = xPos;

      // Shrink bracket for zero of fprime
      if (fFirstDerivTest > 0)
        xPosNew = xTest;
      else
        xNegNew = xTest;

      // Calculate new newton step
      Real xTestNew = xTest - fFirstDerivTest / fSecondDerivTest;

      // If the newton step goes outside the bracket, fall back on bisection
      if (xTestNew > max(xPosNew, xNegNew) || xTestNew < min(xPosNew, xNegNew))
        xTestNew = (xPosNew + xNegNew)/2;

      if (abs(xTestNew - xTest) < searchThresh)
        return xTestNew;
      else
        return findLocalMinimum(f, xNegNew, xPosNew, xTestNew, searchThresh);
    }

    void run(BranchAndBound<F> &bb,
             const Real &localSearchThresh,
             const Real &searchThresh) {
      bb.numIntervalTests++;
      if (xRight - xLeft <= localSearchThresh) {
        // TODO(dsd): Should we be pre-computing these?
        const Real bLeft  = f.derivative1(xLeft);
        const Real bRight = f.derivative1(xRight);
        if (bLeft < 0 && bRight > 0) {
          // Derivative changes sign from negative to positive across
          // interval, so local minimum is in the middle
          bb.numNewtonSearches++;
          const Real xLocalMin =
            findLocalMinimum(f, xLeft, xRight, (xLeft + xRight)/2, searchThresh);
          bb.maybeUpdateGlobalMin(fIndex, xLocalMin, f(xLocalMin));
        } else if (fLeft < fRight) {
          bb.maybeUpdateGlobalMin(fIndex, xLeft, fLeft);
        } else {
          bb.maybeUpdateGlobalMin(fIndex, xRight, fRight);
        }
      } else {
        interval<Real> intervalEstimate = f(interval<Real>(xLeft, xRight));
        if (intervalEstimate.lower() < bb.fGlobalMin) {
          const Real xMid = (xLeft + xRight)/2;
          const Real aMid = f(xMid);
          bb.maybeUpdateGlobalMin(fIndex, xMid, aMid);
          bb.q.push(MinSearch(xLeft, fLeft, xMid,   aMid,   f, fIndex));
          bb.q.push(MinSearch(xMid,  aMid,  xRight, fRight, f, fIndex));
        }
      }
    }
  };

  queue<MinSearch> q;
  BranchAndBound(const Real &searchThresh, const Real &localSearchThresh):
    searchThresh(searchThresh),
    localSearchThresh(localSearchThresh),
    numIntervalTests(0),
    numNewtonSearches(0),
    q() {}

  void enqueueFunction(const F &f, int fIndex) {
    const Real fLeft = f(f.xLeft);
    const Real fRight = f(f.xRight);

    // iGlobalMin < 0 indicates that the minima are un-initialized, in
    // which case we should force-update the global minimum
    maybeUpdateGlobalMin(fIndex, f.xLeft,  fLeft, iGlobalMin < 0);
    maybeUpdateGlobalMin(fIndex, f.xRight, fRight);

    q.push(MinSearch(f.xLeft, fLeft, f.xRight, fRight, f, fIndex));
  }

  boost::tuple<int, Real, Real> minimize() {
    while (!q.empty()) {
      q.front().run(*this, localSearchThresh, searchThresh);
      q.pop();
    }
    return boost::tuple<int, Real, Real>(iGlobalMin, xGlobalMin, fGlobalMin);
  }

  boost::tuple<int, Real, Real> minimizeFunctions(vector<F> fs, Real fInitialMin) {
    // Initialize the global minimum.  *HACK* iGlobalMin = -1
    // indicates that no minimum below fInitialMin has been found yet.
    maybeUpdateGlobalMin(-1, -1, fInitialMin, true);
    for (unsigned int i = 0; i < fs.size(); i++) {
      enqueueFunction(fs[i], i);
    }
    boost::tuple<int, Real, Real> result = minimize();
    return result;
  }
};

#endif  // SIP_SOLVE_BB_H_
