// Author(s): D. Simmons-Duffin, April-October 2013

#ifndef SIP_SOLVE_SERIALIZE_H_
#define SIP_SOLVE_SERIALIZE_H_

#include <string>
#include <vector>
#include "boost/serialization/serialization.hpp"
#include "boost/serialization/base_object.hpp"
#include "boost/serialization/utility.hpp"
#include "boost/serialization/vector.hpp"
#include "boost/serialization/string.hpp"
#include "boost/serialization/split_free.hpp"
#include "boost/archive/text_oarchive.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include "types.h"
#include "SIPSolver.h"
#include "BoundarySearch.h"

using boost::filesystem::path;
using boost::archive::text_iarchive;
using std::vector;

namespace boost {
  namespace serialization {
    template<class Archive>
    void load(Archive& ar, Real& d, unsigned int version) {
      std::string s;
      ar & s;
      d = Real(s.c_str());
    }

    template<class Archive>
    void save(Archive& ar, Real const& d, unsigned int version) {
      std::string s(d.to_string());
      ar & s;
    }

    template<class Archive>
    void serialize(Archive& ar, BasisElt& b, const unsigned int version) {
      ar & b.ty;
      ar & b.n;
      ar & b.x;
    }

    template<class Archive>
    void serialize(Archive& ar, BoundarySearch& b, const unsigned int version) {
      ar & b.xMin;
      ar & b.xMax;
      ar & b.feasibleBasis;
      ar & b.feasibleCoords;
      ar & b.workingBasis;
    }

  }  // namespace serialization
}  // namespace boost

BOOST_SERIALIZATION_SPLIT_FREE(Real)
BOOST_CLASS_VERSION(Real, 0)
BOOST_CLASS_TRACKING(Real,           boost::serialization::track_never)
BOOST_CLASS_TRACKING(BasisElt,       boost::serialization::track_never)
BOOST_CLASS_TRACKING(BoundarySearch, boost::serialization::track_never)

template <class A>
vector<A> loadManyFromArchive(path archivePath) {
  vector<A> as;
  if (exists(archivePath)) {
    boost::filesystem::ifstream ifs(archivePath);
    text_iarchive archive(ifs);
    A a;

    while (true)
      try {
        archive >> a;
        as.push_back(a);
      } catch(boost::archive::archive_exception e) {
        break;
      }
  }
  return as;
}

#endif  // SIP_SOLVE_SERIALIZE_H_
