// Author(s): D. Simmons-Duffin, April-October 2013

#include <vector>
#include "DampedPartialFraction.h"

DampedPartialFraction::DampedPartialFraction() {}

DampedPartialFraction::DampedPartialFraction(Real rho,
                                             Polynomial p,
                                             vector<Real> poles,
                                             vector<Real> residues,
                                             Real xLeft,
                                             Real xRight):
  rho(rho),
  logRho(log(rho)),
  xLeft(xLeft),
  xRight(xRight),
  p0(p),
  poles(poles),
  residues(residues) {
  precomputeDerivatives();
}

Polynomial
DampedPartialFraction::dampedPolynomialDerivative(const Polynomial &q) const {
  int deg = q.degree();
  Polynomial p = q;

  // Derivative of the polynomial part
  for (int i = 0; i <= deg; i++) {
    p.coeffs[i] *= logRho;
    if (i >= 1)
      p.coeffs[i - 1] += i*q.coeffs[i];
  }

  return p;
}

void DampedPartialFraction::precomputeDerivatives() {
  // Precompute vaarious derivatives
  p1 = dampedPolynomialDerivative(p0);
  p2 = dampedPolynomialDerivative(p1);
  p3 = dampedPolynomialDerivative(p2);
}

Real DampedPartialFraction::operator()(const Real &x) const {
  Real total = p0(x);
  int numPoles = poles.size();

  for (int i = 0; i < numPoles; i++)
    total += residues[i]/(x - poles[i]);
  return pow(rho, x) * total;
}

Real DampedPartialFraction::derivative1(const Real &x) const {
  Real total = p1(x);
  int numPoles = poles.size();

  for (int i = 0; i < numPoles; i++)
    total += singlePoleDerivative1(residues[i], poles[i], x);
  return pow(rho, x) * total;
}

Real DampedPartialFraction::derivative2(const Real &x) const {
  Real total = p2(x);
  int numPoles = poles.size();

  for (int i = 0; i < numPoles; i++)
    total += singlePoleDerivative2(residues[i], poles[i], x);
  return pow(rho, x) * total;
}

Real DampedPartialFraction::derivative3(const Real &x) const {
  Real total = p3(x);
  int numPoles = poles.size();

  for (int i = 0; i < numPoles; i++)
    total += singlePoleDerivative3(residues[i], poles[i], x);
  return pow(rho, x) * total;
}

Real DampedPartialFraction::singlePoleDerivative1(const Real &r,
                                                  const Real &a,
                                                  const Real &x) const {
  Real q = 1/(x-a);
  return r*q*(logRho - q);
}

Real DampedPartialFraction::singlePoleDerivative2(const Real &r,
                                                  const Real &a,
                                                  const Real &x) const {
  Real q = 1/(x-a);
  return r*q*(logRho*logRho - 2*q*(logRho - q));
}

Real DampedPartialFraction::singlePoleDerivative3(const Real &r,
                                                  const Real &a,
                                                  const Real &x) const {
  Real q = 1/(x-a);
  Real logRho2 = logRho*logRho;
  return r*q*(logRho2*logRho - 3*q*(logRho2 - 2*q*(logRho - q)));
}

// Computes the first, second, and third derivative simultaneously, with
// d1, d2, d3 as outputs
void DampedPartialFraction::multiDerivatives(const Real &x,
                                             Real &d1,
                                             Real &d2,
                                             Real &d3) const {
  const Real logRho2 = logRho*logRho;
  const Real logRho3 = logRho2*logRho;
  const Real rhoX = pow(rho, x);
  Real r, q, s1, s2, s3;
  int numPoles = poles.size();

  d1 = p1(x);
  d2 = p2(x);
  d3 = p3(x);

  for (int i = 0; i < numPoles; i++) {
    r = residues[i];
    q = 1/(x-poles[i]);
    s1 = q*(logRho  - q);
    s2 = q*(logRho2 - 2*s1);
    s3 = q*(logRho3 - 3*s2);
    d1 += r*s1;
    d2 += r*s2;
    d3 += r*s3;
  }

  d1 *= rhoX;
  d2 *= rhoX;
  d3 *= rhoX;
}

interval<Real>
DampedPartialFraction::operator()(const interval<Real> &x) const {
  interval<Real> total = p0(x);
  int numPoles = poles.size();

  for (int i = 0; i < numPoles; i++)
    total += residues[i]/(x - poles[i]);

  return interval<Real>(pow(rho, x.upper()), pow(rho, x.lower())) * total;
}
