// Author(s): D. Simmons-Duffin, April-October 2013

#include <iostream>
#include <vector>
#include <string>
#include "boost/filesystem.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"
#include "BasisElt.h"
#include "util.h"

using std::string;
using boost::filesystem::path;

void printBasisJson(ostream &out, const vector< pair<BasisElt, Real> >& basis) {
  out << "  [\n";
  for (unsigned int i = 0; i < basis.size(); i++) {
    BasisElt b = basis[i].first;
    Real ope   = basis[i].second;
    out << "    {";
    if (b.ty == SIP_POINT) {
      out << " \"type\": \"point\",";
    } else {
      out << " \"type\": \"curve\",";
      out << " \"x\": " << b.x << ",";
    }
    out << " \"index\": " << b.n << ",";
    out << " \"ope\": " << ope << " }";
    out << (i < basis.size() - 1 ? ",\n" : "\n");
  }
  out << "  ]\n";
}

void printBoundaryJson(ostream &out,
                       const pair<Real, vector< pair<BasisElt, Real> > >& bdry) {
  out << "{\n";
  out << "  \"xBoundary\" : " << bdry.first << ",\n";
  out << "  \"basis\"     :\n";
  printBasisJson(out, bdry.second);
  out << "}";
}

path backupFile(path p) {
  path backup = p;
  boost::posix_time::ptime now = boost::posix_time::second_clock::local_time();
  string nowString = boost::posix_time::to_iso_extended_string(now);
  std::replace(nowString.begin(), nowString.end(), ',', '.');
  backup += "-backup-";
  backup += nowString;
  boost::filesystem::copy_file(p, backup);
  return backup;
}
