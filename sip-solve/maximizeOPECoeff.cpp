// Author(s): D. Simmons-Duffin, April-October 2013

#include <iostream>
#include <fstream>
#include <utility>
#include <vector>
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/function.hpp"
#include "boost/phoenix.hpp"
#include "boost/phoenix/core.hpp"
#include "boost/phoenix/core/reference.hpp"
#include "boost/phoenix/function.hpp"
#include "boost/phoenix/bind/bind_function.hpp"
#include "boost/serialization/utility.hpp"
#include "parse.h"
#include "omp.h"
#include "SIPSolver.h"
#include "serialize.h"
#include "util.h"
#include "maximizeOPECoeff.h"

using std::endl;
using std::cout;
using boost::filesystem::path;
using boost::filesystem::exists;
using boost::archive::text_oarchive;
using boost::phoenix::bind;
using boost::phoenix::ref;
using boost::phoenix::val;
using boost::phoenix::arg_names::arg1;

void saveMaximizationCheckpoint(text_oarchive &checkpointArchive,
                                boost::filesystem::ofstream &checkpoints,
                                const OPEMaximizationStage &stage,
                                const vector<BasisElt> &basis) {
  cout << "Saving checkpoint...";
  const pair<OPEMaximizationStage, vector<BasisElt> > p(stage, basis);
  checkpointArchive << p;
  checkpoints.flush();
  cout << " done." << endl;
}

void maximizeOPECoeff(path sipPath,
                      path outPath,
                      path checkpointPath,
                      int curveIndex,
                      Real gap) {
  SemiInfiniteProgram sip = readSemiInfiniteProgram(sipPath.c_str());

  vector<OPEMaximizationCheckpoint> maximizationCheckpoints =
    loadManyFromArchive<OPEMaximizationCheckpoint>(checkpointPath);

  if (exists(checkpointPath)) {
    path backup = backupFile(checkpointPath);
    cout << "Backed up old checkpoints to : " << backup << endl;
  }
  cout << "Using " << omp_get_max_threads() << " threads" << endl;
  cout << "Found " << maximizationCheckpoints.size() << " checkpoints" << endl;

  boost::filesystem::ofstream checkpoints(checkpointPath);
  text_oarchive checkpointArchive(checkpoints);

  const boost::function<void(const vector<BasisElt>&)> saveCheckpointFindingFeasible =
    bind(&saveMaximizationCheckpoint,
         ref(checkpointArchive),
         ref(checkpoints),
         val(FINDING_FEASIBLE),
         arg1);

  const boost::function<void(const vector<BasisElt>&)> saveCheckpointMaximizingCoefficient =
    bind(&saveMaximizationCheckpoint,
         ref(checkpointArchive),
         ref(checkpoints),
         val(MAXIMIZING_COEFFICIENT),
         arg1);

  if (maximizationCheckpoints.size() > 0) {
    // The last checkpoint will be saved as soon as the algorithm starts
    for (unsigned int i = 0; i < maximizationCheckpoints.size() - 1; i++)
      checkpointArchive << maximizationCheckpoints[i];
  } else {
    maximizationCheckpoints.push_back(pair<OPEMaximizationStage, vector<BasisElt> >(FINDING_FEASIBLE,
                                                                                    vector<BasisElt>(0)));
  }

  OPEMaximizationStage stage    = maximizationCheckpoints.back().first;
  vector<BasisElt> workingBasis = maximizationCheckpoints.back().second;

  if (stage == FINDING_FEASIBLE) {
    SIPSolver feasibleSolver = SIPSolver::feasibility(sip, workingBasis);

    Real obj = feasibleSolver.run(MAX_ITERATIONS,
                                  saveCheckpointFindingFeasible);
    if (obj > 0)
      throw("Couldn't find initial feasible solution");

    workingBasis = feasibleSolver.basis;
    saveCheckpointMaximizingCoefficient(workingBasis);
  }

  cout << "\n=======================================================\n";
  cout << "Found feasible basis:\n";
  printBasis(cout, workingBasis);

  SemiInfiniteProgram opeMaximizeSIP = sip;
  vector<Real> operatorVector = sip.curves[curveIndex](gap);
  opeMaximizeSIP.points.push_back(operatorVector);
  opeMaximizeSIP.pointObj.push_back(-1);
  SIPSolver opeMaximizeSolver(opeMaximizeSIP, workingBasis);

  Real opeMax = opeMaximizeSolver.run(MAX_ITERATIONS,
                                      saveCheckpointMaximizingCoefficient);
  saveCheckpointMaximizingCoefficient(opeMaximizeSolver.basis);
  checkpoints.close();

  cout << "\n*******************************************************" << endl;
  cout << "maximum ope coefficient: " << opeMax << "\n";
  cout << "basis: \n";
  printBasis(cout, opeMaximizeSolver.basis);

  boost::filesystem::ofstream outStream(outPath);
  outStream.precision(30);
  printBasisJson(outStream, zipVectors(opeMaximizeSolver.basis,
                                       opeMaximizeSolver.coefficients()));
  outStream.close();
}
